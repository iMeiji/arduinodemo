火焰报警实验

下面是本次模拟实验所用到的元件：   

有源蜂鸣器  x  1
火焰传感器  x  1
10K电阻      x  1

当然还有控制板、面包板和导线等等。
下面是我们的电路连接图（蜂鸣器正极连接D12，负极接GND；火焰传感器负极连接+5v，正极连接A5，通过10K电阻连接到GND）：


dht11共有四只脚，左边第一只脚接+5v，第二只脚接arduino pin2，第四只脚接地，比较特别的是第一、二只脚还要用一个4.7k的电阻连接，

esp8266 RX接pin9，TX接pin8，其他按着图连