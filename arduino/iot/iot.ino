#include <SoftwareSerial.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x20, 16, 2); // set the LCD address to 0x20 for a 16 chars and 2 line display

//初始化 WiFi
#define WIFI_TX       9
#define WIFI_RX       8
#define LED           13
SoftwareSerial wifi(WIFI_RX, WIFI_TX);   //RX, TX

String _comdata_wifi = "";             //WiFi串口的数据

//初始化温湿度
int temp;//温度
int humi;//湿度
int tol;//校对码
int j;
int a = 1;
unsigned int loopCnt;
int chr[40] = {0};//创建数字数组，用来存放40个bit
unsigned long time;
#define pin 2

//初始化火焰和蜂鸣器
int Flame = A5;   //定义火焰接口为模拟5接口
int Buzzer = 12;  //定义蜂鸣器接口为数字12接口
boolean isBee;

void setup() {
  pinMode(Buzzer, OUTPUT);
  Serial.begin(9600);
  wifi.begin(115200);
  Serial.println("system is ready!");

  wifi.println("AT+CWMODE=3\r\n");
  delay(500);
  wifi.println("AT+CIPMUX=1\r\n");
  delay(500);
  wifi.println("AT+CIPSERVER=1,5000\r\n");
  delay(500);
}

void loop() {


  getWifiSerialData();
  if ( (_comdata_wifi != "") ) {
    // +IPD,0,6:133456
    Serial.println("Receive");
    //    Serial.println(_comdata_wifi);
    if ((_comdata_wifi[2] == '+') && (_comdata_wifi[3] == 'I') && (_comdata_wifi[4] == 'P')) {
      if ((_comdata_wifi[5] == 'D') && (_comdata_wifi[8] == ',')) {
        // 判断客户端发送回来的数据
        if (_comdata_wifi[11] == '1') {
          Serial.println("OPEN");
          isBee = true;
        }
        else if (_comdata_wifi[11] == '0') {
          Serial.println("CLOSE");
          isBee = false;
        }
      }
    }
    liduid();
    _comdata_wifi = String("");
  }
  Bee();
}

void Bee() {
  int i, j;
  if (isBee) {
    for (i = 0; i < 40; i++) //辒出一个频率的声音
    {
      digitalWrite(Buzzer, HIGH); //发声音
      delay(1);//延时1ms
      digitalWrite(Buzzer, LOW); //不发声音
      delay(1);//延时ms
    }
    for (i = 0; i < 40; i++) //辒出另一个频率癿声音
    {
      digitalWrite(Buzzer, HIGH); //发声音
      delay(2);//延时2ms
      digitalWrite(Buzzer, LOW); //不发声音
      delay(2);//延时2ms
    }
  }
}

void getWifiSerialData() {
  while (wifi.available() ) {
    _comdata_wifi += char(wifi.read());   //get wifi data
    delay(4);
  }
}

void liduid() {
bgn:
  delay(2000);
  //设置2号接口模式为：输出
  //输出低电平20ms（>18ms）
  //输出高电平40μs
  pinMode(pin, OUTPUT);
  digitalWrite(pin, LOW);
  delay(20);
  digitalWrite(pin, HIGH);
  delayMicroseconds(40);
  digitalWrite(pin, LOW);
  //设置2号接口模式：输入
  pinMode(pin, INPUT);
  //高电平响应信号
  loopCnt = 10000;
  while (digitalRead(pin) != HIGH)
  {
    if (loopCnt-- == 0)
    {
      //如果长时间不返回高电平，输出个提示，重头开始。
      Serial.println("HIGH");
      goto bgn;
    }
  }
  //低电平响应信号
  loopCnt = 30000;
  while (digitalRead(pin) != LOW)
  {
    if (loopCnt-- == 0)
    {
      //如果长时间不返回低电平，输出个提示，重头开始。
      Serial.println("LOW");
      goto bgn;
    }
  }
  //开始读取bit1-40的数值
  for (int i = 0; i < 40; i++)
  {
    while (digitalRead(pin) == LOW)
    {}
    //当出现高电平时，记下时间“time”
    time = micros();
    while (digitalRead(pin) == HIGH)
    {}
    //当出现低电平，记下时间，再减去刚才储存的time
    //得出的值若大于50μs，则为‘1’，否则为‘0’
    //并储存到数组里去
    if (micros() - time > 50)
    {
      chr[i] = 1;
    } else {
      chr[i] = 0;
    }
  }

  //湿度，8位的bit，转换为数值
  humi = chr[0] * 128 + chr[1] * 64 + chr[2] * 32 + chr[3] * 16 + chr[4] * 8 + chr[5] * 4 + chr[6] * 2 + chr[7];

  //温度，8位的bit，转换为数值
  temp = chr[16] * 128 + chr[17] * 64 + chr[18] * 32 + chr[19] * 16 + chr[20] * 8 + chr[21] * 4 + chr[22] * 2 + chr[23];
  //校对码，8位的bit，转换为数值
  //  tol = chr[32] * 128 + chr[33] * 64 + chr[34] * 32 + chr[35] * 16 + chr[36] * 8 + chr[37] * 4 + chr[38] * 2 + chr[39];
  //输出：温度、湿度、校对码
  String sTemp = "temp=";
  sTemp.concat(temp);
  //  Serial.println(sTemp);

  String sHumi = "humi=";
  sHumi.concat(humi);
  //  Serial.println(sHumi);

  int fire = analogRead(Flame); //读取火焰传感器的模拟值
  if (fire > 100) {
    isBee = true;
  }
  String sFire = "fire=";
  sFire.concat(fire);
  Serial.println(sTemp + "," + sHumi + "," + sFire); //输出模拟值，并将其打印出来
  //  String sTol = "tol=";
  //  sTol.concat(tol);
  //  Serial.println(sTol);
  //校对码，我这里没用上
  //理论上，湿度+温度=校对码
  //如果数值不相等，说明读取的数据有错。
  //  lcd.init();         // 初始化LCD
  //  lcd.clear();        //清LCD屏
  //  lcd.backlight();    //亮LCD
  //  lcd.print("temp:"); lcd.print(temp);
  //  lcd.setCursor(0, 1);
  //  lcd.print("humi:"); lcd.print(humi);
  delay(200);
  wifi.println("AT+CIPSEND=0,24");
  delay(200);
  wifi.println(sTemp + "," + sHumi + "," + sFire); // temp=31,humi=46,fire=10
  delay(200);
  wifi.flush();
  Serial.flush();
  delay(200);
}
