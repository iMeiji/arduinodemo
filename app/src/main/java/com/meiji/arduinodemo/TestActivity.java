package com.meiji.arduinodemo;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class TestActivity extends AppCompatActivity {

    private static final String TAG = "TestActivity";
    ArrayList<String> mTempXvalues = new ArrayList<>();
    ArrayList<BarEntry> mTempYvalues = new ArrayList<>();
    ArrayList<BarDataSet> mTempBarDataSets = new ArrayList<>();
    int mTempCount;
    private BarChart mBarChart;
    private BarDataSet barDataSet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
        initView();

//        showBarChart(mBarChart);

        setupTempChart(mBarChart);
    }

    private void showBarChart(BarChart barChart) {
        // 如果没有数据的时候，会显示这个，类似ListView的EmptyView
        barChart.setNoDataTextDescription("You need to provide data for the chart.");

//        barChart.setData(barData); // 设置数据

        barChart.setDrawBorders(false); //是否在折线图上添加边框

        barChart.setDescription("数据描述");// 数据描述
        barChart.setDescriptionPosition(100, 20);//数据描述的位置
        barChart.setDescriptionColor(Color.RED);//数据的颜色
        barChart.setDescriptionTextSize(40);//数据字体大小

        barChart.setDrawGridBackground(false); // 是否显示表格颜色
        barChart.setGridBackgroundColor(Color.RED); // 表格的的颜色
        //barChart.setBackgroundColor(Color.BLACK);// 设置整个图标控件的背景
        barChart.setDrawBarShadow(false);//柱状图没有数据的部分是否显示阴影效果

        barChart.setTouchEnabled(true); // 设置是否可以触摸
        barChart.setDragEnabled(true);// 是否可以拖拽
        barChart.setScaleEnabled(true);// 是否可以缩放
        barChart.setPinchZoom(false);//y轴的值是否跟随图表变换缩放;如果禁止，y轴的值会跟随图表变换缩放

        barChart.setDrawValueAboveBar(true);//柱状图上面的数值显示在柱子上面还是柱子里面

        barChart.getXAxis().setDrawGridLines(false);//是否显示竖直标尺线
        barChart.getXAxis().setLabelsToSkip(11);//设置横坐标显示的间隔
//        barChart.getXAxis().setLabelRotationAngle(20);//设置横坐标倾斜角度
//        barChart.getXAxis().setSpaceBetweenLabels(50);
        barChart.getXAxis().setDrawLabels(true);//是否显示X轴数值
        barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);//设置X轴的位置 默认在上方
        barChart.getXAxis().setDrawAxisLine(true);

        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                Log.w("TAG", dataSetIndex + "dataSetIndex" + "h" + h + "-----" + e);
                barDataSet.setDrawValues(true);
            }

            @Override
            public void onNothingSelected() {
                barDataSet.setDrawValues(false);
            }
        });

        barChart.getAxisRight().setDrawLabels(false);//右侧是否显示Y轴数值
        barChart.getAxisRight().setEnabled(false);//是否显示最右侧竖线
        barChart.getAxisRight().setDrawAxisLine(true);
        barChart.getAxisLeft().setDrawAxisLine(false);

        barChart.getAxisLeft().setLabelCount(2, false);

        YAxisValueFormatter custom = new MyYAxisValueFormatter();//自定义Y轴文字样式
        barChart.getAxisLeft().setValueFormatter(custom);

        barChart.getLegend().setPosition(Legend.LegendPosition.RIGHT_OF_CHART);//设置比例图标的位置
        barChart.getLegend().setDirection(Legend.LegendDirection.RIGHT_TO_LEFT);//设置比例图标和文字之间的位置方向
        barChart.getLegend().setTextColor(Color.RED);

//        barChart.animateXY(2000, 3000);
    }

    private void updateBarData(int count, float range) {
        ArrayList<String> xValues = new ArrayList<String>();
        for (int i = 0; i < count; i++) {
            xValues.add(i + "");
        }

        ArrayList<BarEntry> yValues = new ArrayList<BarEntry>();

        for (int i = 0; i < count; i++) {
            yValues.add(new BarEntry(range, i));
        }

        // y轴的数据集合
        barDataSet = new BarDataSet(yValues, "collection");

        ArrayList<BarDataSet> barDataSets = new ArrayList<BarDataSet>();
        barDataSets.add(barDataSet); // add the datasets

        BarData barData = new BarData(xValues, barDataSet);

        mBarChart.setData(barData);
        mBarChart.invalidate();
    }

    @Override
    public void onResume() {
        super.onResume();
        // test
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                App.appExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
//                        addTempBarData(++mTempCount, mTempCount);
                        updateBarData(++mTempCount, 33);
                    }
                });
            }
        }, 500, 500);
    }

    private void initView() {
        mBarChart = findViewById(R.id.chartHumi);
    }

    private void setupTempChart(BarChart chart) {
        chart.setDrawGridBackground(false);
        chart.setDescription("温度变化图表");
        chart.setNoDataTextDescription("暂无数据");
//        chart.setVisibleXRangeMaximum(9);
//        chart.setVisibleYRangeMaximum(60, YAxis.AxisDependency.LEFT);

        // 为chart添加空数据
        chart.setData(new BarData());

        // 设置x轴
        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(4);

        // 设置左侧坐标轴
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);

        // 设置右侧坐标轴
        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(false);
    }

    private void addTempBarData(int count, float range) {
        Log.d(TAG, "addTempBarData: " + count + " - " + range);

        mTempXvalues.add(count + "");

        // y轴的数据集合
        for (int i = 0; i < count; i++) {

            mTempYvalues.add(new BarEntry(range, (int) range));
        }
        barDataSet = new BarDataSet(mTempYvalues, "数值");

        mTempBarDataSets.add(barDataSet); // add the datasets

        BarData barData = new BarData(mTempXvalues, barDataSet);
        mBarChart.setData(barData); // 设置数据
        mBarChart.invalidate();
    }

    public class MyYAxisValueFormatter implements YAxisValueFormatter {

        private DecimalFormat mFormat;

        public MyYAxisValueFormatter() {
            mFormat = new DecimalFormat("###,###,###,##0");
        }

        @Override
        public String getFormattedValue(float value, YAxis yAxis) {
            return mFormat.format(value) + "K";
        }
    }
}
