package com.meiji.arduinodemo.okb;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.meiji.arduinodemo.AppService;
import com.meiji.arduinodemo.R;
import com.meiji.arduinodemo.StringUtil;

import java.util.ArrayList;

import static android.content.Context.NOTIFICATION_SERVICE;
import static com.meiji.arduinodemo.meiji.TempFragment.TEMP_IntentFilter;

/**
 * Created by Meiji on 2018/1/6.
 */

public class OkbFireFragment extends Fragment {

    private static final String TAG = "OkbFireFragment";
    static OkbFireFragment INSTANCE;
    TextView tv_fire;
    BarChart chartFire;
    Button bt_openalarm;
    Button bt_closealarm;
    TextView tv_alarm;
    LocalReceiver mLocalReceiver;
    AppService.AppBinder mBinder;
    ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBinder = (AppService.AppBinder) service;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    int mFireCount;

    public static OkbFireFragment newInstance() {
        if (INSTANCE == null) {
                INSTANCE = new OkbFireFragment();
        }
        return INSTANCE;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.okb_fragment_fire, container, false);
        initView(view);
        initBC();
        return view;
    }

    void initView(View view) {
        this.tv_fire = view.findViewById(R.id.tv_fire);
        this.chartFire = view.findViewById(R.id.chartFire);
        this.bt_openalarm = view.findViewById(R.id.bt_openalarm);
        this.bt_closealarm = view.findViewById(R.id.bt_closealarm);
        this.tv_alarm = view.findViewById(R.id.tv_alarm);
        setupTempChart(chartFire);
        bt_openalarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinder.sendMessage("1");
                tv_alarm.setText("当前警报状态：开启");
            }
        });

        bt_closealarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinder.sendMessage("0");
                tv_alarm.setText("当前警报状态：关闭");
            }
        });
    }

    void initBC() {

        Intent intent = new Intent(getContext(), AppService.class);
        getContext().bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
        mLocalReceiver = new LocalReceiver();
        LocalBroadcastManager.getInstance(getContext())
                .registerReceiver(mLocalReceiver, new IntentFilter(TEMP_IntentFilter));
    }

    @Override
    public void onDestroy() {
        getContext().unbindService(mServiceConnection);
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mLocalReceiver);
        super.onDestroy();
    }

    private void setupTempChart(BarChart chart) {
        chart.setDrawGridBackground(false);
        chart.setDescription("火焰值变化图表");
        chart.setNoDataTextDescription("暂无数据");
//        chart.setVisibleXRangeMaximum(9);
//        chart.setVisibleYRangeMaximum(150, YAxis.AxisDependency.LEFT);

        // 为chart添加空数据
        chart.setData(new BarData());

        // 设置x轴
        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(4);

        // 设置左侧坐标轴
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);

        // 设置右侧坐标轴
        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(false);
    }

    private void updateTempBarData(int count, float range) {
        ArrayList<String> xValues = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            xValues.add(i + "");
        }

        ArrayList<BarEntry> yValues = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            yValues.add(new BarEntry(range, i));
        }

        // y轴的数据集合
        BarDataSet barDataSet = new BarDataSet(yValues, "数值");

        ArrayList<BarDataSet> barDataSets = new ArrayList<>();
        barDataSets.add(barDataSet); // add the datasets

        BarData barData = new BarData(xValues, barDataSet);

        chartFire.setData(barData);
        chartFire.invalidate();
    }

    void showNotification() {

        NotificationManager mNotifyMgr =
                (NotificationManager) getContext().getSystemService(NOTIFICATION_SERVICE);
//        PendingIntent contentIntent = PendingIntent.getActivity(
//                this, 0, new Intent(this, ResultActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getContext())
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentTitle("警报！！！")
                        .setContentText("火焰值过高，已启动警报")
                        .setFullScreenIntent(null, false);

        mNotifyMgr.notify(0, mBuilder.build());

    }

    public void callPhone(String phoneNum) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        Uri data = Uri.parse("tel:" + phoneNum);
        intent.setData(data);
        startActivity(intent);
    }

    public class LocalReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String response = intent.getStringExtra(AppService.EXTRA_RESPONSE_DATA);
                if (!TextUtils.isEmpty(response)) {
                    // temp=21,humi=56,fire=1
                    for (String s : response.split(",")) {
                        if (s.contains("fire")) {
                            String temp = s.substring(s.indexOf("=") + 1, s.length());
                            tv_fire.setText("当前火焰值 " + temp);
                            int fire = StringUtil.getNum(temp);
                            updateTempBarData(++mFireCount, fire);
                            Log.d(TAG, "onReceive: fire = " + fire);

                            if (fire > 50) {
//                                showNotification();
                                MaterialDialog.Builder dialog = new MaterialDialog.Builder(context);
                                dialog.title("已着火");
                                dialog.content("当前火焰值 " + temp);
                                MaterialDialog build = dialog.build();
                                build.setActionButton(DialogAction.POSITIVE, "确定报警");
                                build.getActionButton(DialogAction.POSITIVE).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callPhone("119");
                                    }
                                });
                                build.show();
                                tv_alarm.setText("当前警报状态：开启");
                            }
                        }
                    }
                }
                Log.d(TAG, "onReceive: " + response);
            }
            Log.e(TAG, "接收到了本地广播");
        }
    }
}
