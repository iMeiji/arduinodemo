package com.meiji.arduinodemo.okb;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.meiji.arduinodemo.App;
import com.meiji.arduinodemo.AppService;
import com.meiji.arduinodemo.R;
import com.meiji.arduinodemo.StringUtil;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A placeholder fragment containing a simple view.
 */
public class OkbTempFragment extends Fragment {

    public static final String TEMP_IntentFilter = "com.meiji.arduinodemo.tempfragment";
    private static final String TAG = "OkbTempFragment";
    static OkbTempFragment INSTANCE;
    // 显示接收服务器消息 按钮
    TextView tv_temp;
    TextView tv_humi;
    BarChart chartTemp;
    BarChart chartHumi;
    LocalReceiver mLocalReceiver;

    int mTempCount;
    int mHumiCount;

    AppService.AppBinder mBinder;
    ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBinder = (AppService.AppBinder) service;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    public static OkbTempFragment newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new OkbTempFragment();
        }
        return INSTANCE;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.okb_fragment_temp, container, false);
        initView(view);
        initBC();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        // test
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                App.appExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
//                        updateTempBarData(++mTempCount, 30);
//                        updateHumiBarData(++mTempCount, 50);
                    }
                });
            }
        }, 500, 500);
    }

    void initBC() {
        Intent intent = new Intent(getContext(), AppService.class);
        getContext().bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
        mLocalReceiver = new LocalReceiver();
        LocalBroadcastManager.getInstance(getContext())
                .registerReceiver(mLocalReceiver, new IntentFilter(TEMP_IntentFilter));
    }


    @Override
    public void onDestroy() {
        getContext().unbindService(mServiceConnection);
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mLocalReceiver);
        super.onDestroy();
    }

    private void initView(View view) {
//        setHasOptionsMenu(true);
        this.tv_temp = view.findViewById(R.id.tv_temp);
        this.tv_humi = view.findViewById(R.id.tv_humi);
        this.chartTemp = view.findViewById(R.id.chartTemp);
        this.chartHumi = view.findViewById(R.id.chartHumi);
        setupTempChart(chartTemp);
        setupHumiChart(chartHumi);
    }

    private void updateHumiBarData(int count, float range) {
        ArrayList<String> xValues = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            xValues.add(i + "");
        }

        ArrayList<BarEntry> yValues = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            yValues.add(new BarEntry(range, i));
        }

        // y轴的数据集合
        BarDataSet barDataSet = new BarDataSet(yValues, "数值");

        ArrayList<BarDataSet> barDataSets = new ArrayList<>();
        barDataSets.add(barDataSet); // add the datasets

        BarData barData = new BarData(xValues, barDataSet);

        chartHumi.setData(barData);
        chartHumi.invalidate();
    }

    private void updateTempBarData(int count, float range) {
        ArrayList<String> xValues = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            xValues.add(i + "");
        }

        ArrayList<BarEntry> yValues = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            yValues.add(new BarEntry(range, i));
        }

        // y轴的数据集合
        BarDataSet barDataSet = new BarDataSet(yValues, "数值");

        ArrayList<BarDataSet> barDataSets = new ArrayList<>();
        barDataSets.add(barDataSet); // add the datasets

        BarData barData = new BarData(xValues, barDataSet);

        chartTemp.setData(barData);
        chartTemp.invalidate();
    }

    private void setupTempChart(BarChart chart) {
        chart.setDrawGridBackground(false);
        chart.setDescription("温度变化图表");
        chart.setNoDataTextDescription("暂无数据");
//        chart.setVisibleXRangeMaximum(9);
//        chart.setVisibleYRangeMaximum(150, YAxis.AxisDependency.LEFT);

        // 为chart添加空数据
        chart.setData(new BarData());

        // 设置x轴
        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(4);

        // 设置左侧坐标轴
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);

        // 设置右侧坐标轴
        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(false);
    }

    private void setupHumiChart(BarChart chart) {
        chart.setDrawGridBackground(false);
        chart.setDescription("湿度变化图表");
        chart.setNoDataTextDescription("暂无数据");
//        chart.setVisibleXRangeMaximum(9);
//        chart.setVisibleYRangeMaximum(150, YAxis.AxisDependency.LEFT);

        // 为chart添加空数据
        chart.setData(new BarData());

        // 设置x轴
        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(4);

        // 设置左侧坐标轴
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);

        // 设置右侧坐标轴
        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(false);
    }

    public class LocalReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String response = intent.getStringExtra(AppService.EXTRA_RESPONSE_DATA);
                if (!TextUtils.isEmpty(response)) {
                    // temp=21,humi=56,fire=1
                    for (String s : response.split(",")) {
                        if (s.contains("temp")) {
                            String temp = s.substring(s.indexOf("=") + 1, s.length());
                            tv_temp.setText("当前温度 " + temp + "°C");
                            int i = StringUtil.getNum(temp);
                            updateTempBarData(++mTempCount, i);

                            if (i > OkbMainActivity.mTempValue) {
                                mBinder.sendMessage("1");
                            }
                        }

                        if (s.contains("humi")) {
                            String hmui = s.substring(s.indexOf("=") + 1, s.length());
                            tv_humi.setText("当前湿度 " + hmui + "°F");
                            int h = StringUtil.getNum(hmui);
                            updateHumiBarData(++mHumiCount, h);

                            if (h > OkbMainActivity.mHumiValue) {
                                mBinder.sendMessage("1");
                            }
                        }
                    }
                }
                Log.d(TAG, "onReceive: " + response);
            }
            Log.e(TAG, "接收到了本地广播");
        }
    }

}
