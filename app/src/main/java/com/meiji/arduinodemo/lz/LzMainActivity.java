package com.meiji.arduinodemo.lz;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.meiji.arduinodemo.AppService;
import com.meiji.arduinodemo.R;

/**
 * Created by Meiji on 2018/1/1.
 */

public class LzMainActivity extends AppCompatActivity {

    public static final String LZ_MAIN_INTENT_FILTER = "com.meiji.arduinodemo.lz.mainreceiver";

    final String mServerIP = "192.168.4.1";
    final int mServerPort = 5000;
    public LzAppService.AppBinder mBinder;
    LocalReceiver mLocalReceiver;
    ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBinder = (LzAppService.AppBinder) service;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };
    private RelativeLayout root;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lz_activity_main);
        initService();
        initView();
    }

    private void initView() {
        root = findViewById(R.id.root);
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinder.connect();
            }
        });
    }

    @Override
    protected void onDestroy() {
        unbindService(mServiceConnection);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mLocalReceiver);
        super.onDestroy();
    }

    void initService() {
        Intent intent = new Intent(this, LzAppService.class);
        intent.putExtra(AppService.EXTRA_IP, mServerIP);
        intent.putExtra(AppService.EXTRA_PORT, mServerPort);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);

        mLocalReceiver = new LocalReceiver();
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mLocalReceiver, new IntentFilter(LZ_MAIN_INTENT_FILTER));
    }

    class LocalReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                boolean connect = intent.getBooleanExtra("connect", false);
                if (connect) {
                    Toast.makeText(context, "连接成功", Toast.LENGTH_SHORT).show();
                    LzSecondActivity.start(LzMainActivity.this);
                } else {
                    Toast.makeText(context, "连接失败", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
