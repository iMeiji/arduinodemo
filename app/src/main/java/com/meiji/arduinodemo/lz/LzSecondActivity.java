package com.meiji.arduinodemo.lz;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.meiji.arduinodemo.R;

/**
 * Created by Meiji on 2018/1/1.
 */

public class LzSecondActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String LZ_SECOND_INTENT_FILTER = "com.meiji.arduinodemo.lz.secondreceiver";
    LzAppService.AppBinder mBinder;
    LocalReceiver mLocalReceiver;
    ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBinder = (LzAppService.AppBinder) service;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };
    private TextView tv_cm;
    private Button bt_start;
    private Button bt_end;
    private ImageView iv;
    private TextView tv_desc;

    public static void start(Context context) {
        Intent starter = new Intent(context, LzSecondActivity.class);
        context.startActivity(starter);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lz_activity_second);
        initView();
        mLocalReceiver = new LocalReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(mLocalReceiver, new IntentFilter(LZ_SECOND_INTENT_FILTER));
        Intent intent = new Intent(this, LzAppService.class);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mLocalReceiver);
        unbindService(mServiceConnection);
        super.onDestroy();
    }

    private void initView() {
        tv_cm = findViewById(R.id.tv_cm);
        bt_start = findViewById(R.id.bt_start);
        bt_end = findViewById(R.id.bt_end);

        bt_start.setOnClickListener(this);
        bt_end.setOnClickListener(this);
        iv = findViewById(R.id.iv);
        tv_desc = findViewById(R.id.tv_desc);
        getSupportActionBar().setTitle("倒车系统");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_start:
                mBinder.sendMessage("1");
                break;
            case R.id.bt_end:
                mBinder.sendMessage("0");
                break;
        }
    }

    int getNum(String str) {
        String str2 = "";
        if (str != null && !"".equals(str)) {
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) >= 48 && str.charAt(i) <= 57) {
                    str2 += str.charAt(i);
                }
            }
        }
        return Integer.parseInt(str2);
    }

    class LocalReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String s = intent.getStringExtra(LzAppService.EXTRA_RESPONSE_DATA);
                int cm = getNum(s);
                tv_cm.setText("距离：" + cm + "cm");
                //252������������������������������������������
                System.out.println(cm);
                if (cm > 100) {
                    iv.setImageResource(R.drawable.green);
                    tv_desc.setText("距离较远，处于安全状态");
                } else if (cm > 30) {
                    iv.setImageResource(R.drawable.blue);
                    tv_desc.setText("距离较近，请注意");
                } else {
                    iv.setImageResource(R.drawable.red);
                    tv_desc.setText("危险，请停止倒车");
                }
            }
        }
    }
}
