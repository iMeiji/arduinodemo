package com.meiji.arduinodemo;

/**
 * Created by Meiji on 2018/1/6.
 */

public class StringUtil {

    public static int getNum(String str) {
        String str2 = "";
        if (str != null && !"".equals(str)) {
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) >= 48 && str.charAt(i) <= 57) {
                    str2 += str.charAt(i);
                }
            }
        }
        return Integer.parseInt(str2);
    }
}
