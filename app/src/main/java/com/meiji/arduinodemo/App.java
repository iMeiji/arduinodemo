package com.meiji.arduinodemo;

import android.app.Application;

import java.util.concurrent.Executors;

/**
 * Created by Meiji on 2017/12/31.
 */

public class App extends Application {

    public static AppExecutors appExecutors;

    @Override
    public void onCreate() {
        super.onCreate();
        appExecutors = new AppExecutors(new AppExecutors.DiskIOThreadExecutor(),
                Executors.newFixedThreadPool(AppExecutors.THREAD_COUNT),
                new AppExecutors.MainThreadExecutor());
    }
}
