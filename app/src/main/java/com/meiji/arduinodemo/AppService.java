package com.meiji.arduinodemo;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;

import static com.meiji.arduinodemo.meiji.TempFragment.TEMP_IntentFilter;

/**
 * Created by Meiji on 2017/12/31.
 */

public class AppService extends Service {

    public static final String EXTRA_IP = "mServerIP";
    public static final String EXTRA_PORT = "mServerPort";
    public static final String EXTRA_RESPONSE_DATA = "mResponseData";
    private static final String TAG = "AppService";
    /**
     * 接收服务器消息 变量
     */
    // 输入流对象
    InputStream is;
    // 输入流读取器对象
    InputStreamReader isr;
    BufferedReader br;
    // 接收服务器发送过来的消息
    String response;
    /**
     * 发送消息到服务器 变量
     */
    // 输出流对象
    OutputStream outputStream;
    Timer mTimer = new Timer();
    private volatile Socket mSocket;
    private String mServerIP;
    private int mServerPort;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        if (intent != null) {
            mServerIP = intent.getStringExtra(EXTRA_IP);
            mServerPort = intent.getIntExtra(EXTRA_PORT, 5000);
        }
        return new AppBinder(mTimer);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        mTimer.cancel();
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public class AppBinder extends Binder {

        Timer mTimer;

        public AppBinder(Timer timer) {
            this.mTimer = timer;
        }

        public void connect() {
            /**
             * 创建客户端 & 服务器的连接
             */
            App.appExecutors.networkIO().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.d(TAG, "connect run: ");
                        // 创建Socket对象 & 指定服务端的IP 及 端口号
                        mSocket = new Socket(mServerIP, mServerPort);
                        // 判断客户端和服务器是否连接成功
                        final boolean connected = mSocket.isConnected();
                        App.appExecutors.mainThread().execute(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(AppService.this, connected + "", Toast.LENGTH_SHORT).show();
                            }
                        });
                        if (connected && mSocket != null) {
                            mTimer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    receiverMessage();
                                }
                            }, 900, 1000);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        public void disConnect() {
            /**
             * 断开客户端 & 服务器的连接
             */
            App.appExecutors.networkIO().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        // 断开 客户端发送到服务器 的连接，即关闭输出流对象OutputStream
                        if (outputStream != null) {
                            outputStream.close();
                        }
                        // 断开 服务器发送到客户端 的连接，即关闭输入流读取器对象BufferedReader
                        if (br != null) {
                            br.close();
                        }
                        // 最终关闭整个Socket连接
                        if (mSocket != null) {
                            mSocket.close();
                            // 判断客户端和服务器是否已经断开连接
                            App.appExecutors.mainThread().execute(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(AppService.this, mSocket.isConnected() + "", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        public void reConnect() {
            disConnect();
            connect();
        }

        public void sendMessage(final String message) {
            /**
             * 发送消息 给 服务器
             */
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (mSocket != null && mSocket.isConnected()) {
                            Log.d(TAG, "sendMessage: ");
                            // 步骤1：从Socket 获得输出流对象OutputStream
                            // 该对象作用：发送数据
                            outputStream = mSocket.getOutputStream();
                            // 步骤2：写入需要发送的数据到输出流对象中
                            outputStream.write(message.getBytes("utf-8"));
//                        outputStream.write((mEdit.getText().toString() + "\n").getBytes("utf-8"));
                            // 特别注意：数据的结尾加上换行符才可让服务器端的readline()停止阻塞
                            // 步骤3：发送数据到服务端
                            outputStream.flush();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

        public void receiverMessage() {
            App.appExecutors.networkIO().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (mSocket != null) {
                            Log.d(TAG, "receiverMessage run: ");
                            // 步骤1：创建输入流对象InputStream
                            is = mSocket.getInputStream();
                            // 步骤2：创建输入流读取器对象 并传入输入流对象
                            // 该对象作用：获取服务器返回的数据
                            isr = new InputStreamReader(is);
                            br = new BufferedReader(isr);
//                            // 步骤3：通过输入流读取器对象 接收服务器发送过来的数据
//                        response = br.readLine();
                            char[] chars = new char[24];
                            br.read(chars);
                            response = new String(chars);

                            // 返回来的数据
                            Intent intent = new Intent(TEMP_IntentFilter);
                            intent.putExtra(EXTRA_RESPONSE_DATA, response);
                            LocalBroadcastManager.getInstance(AppService.this).sendBroadcast(intent);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
