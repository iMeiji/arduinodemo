package com.meiji.arduinodemo.meiji;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.meiji.arduinodemo.AppService;
import com.meiji.arduinodemo.R;
import com.meiji.arduinodemo.StringUtil;

/**
 * A placeholder fragment containing a simple view.
 */
public class TempFragment extends Fragment {

    public static final String TEMP_IntentFilter = "com.meiji.arduinodemo.tempfragment";
    private static final String TAG = "TempFragment";
    static TempFragment INSTANCE;
    // 显示接收服务器消息 按钮
    TextView tv_temp;
    TextView tv_humi;
    LineChart chartTemp;
    LineChart chartHumi;
    LocalReceiver mLocalReceiver;

    public static TempFragment newInstance() {
        if (INSTANCE == null) {
            synchronized (TempFragment.class) {
                INSTANCE = new TempFragment();
            }
        }
        return INSTANCE;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_temp, container, false);
        initView(view);
        initBC();

        return view;
    }

    void initBC() {
        mLocalReceiver = new LocalReceiver();
        LocalBroadcastManager.getInstance(getContext())
                .registerReceiver(mLocalReceiver, new IntentFilter(TEMP_IntentFilter));
    }


    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mLocalReceiver);
        super.onDestroy();
    }

    private void initView(View view) {
//        setHasOptionsMenu(true);
        this.tv_temp = view.findViewById(R.id.tv_temp);
        this.tv_humi = view.findViewById(R.id.tv_humi);
        this.chartTemp = view.findViewById(R.id.chartTemp);
        this.chartHumi = view.findViewById(R.id.chartHumi);
        setupTempChart(chartTemp);
        setupHumiChart(chartHumi);
    }

    public void addEntry(LineChart chart, float yValue) {
        LineData lineData = chart.getData();

        if (lineData != null) {
            int indexLast = getLastDataSetIndex(lineData);
            LineDataSet lastSet = (LineDataSet) lineData.getDataSetByIndex(indexLast);
            // set.addEntry(...); // can be called as well

            if (lastSet == null) {
                lastSet = createLineDataSet();
                lineData.addDataSet(lastSet);
            }
            // 这里要注意，x轴的index是从零开始的
            // 假设index=2，那么getEntryCount()就等于3了
            int count = lastSet.getEntryCount();
            if (count > 20) {
                return;
            }
            // add a new x-value first 这行代码不能少
            lineData.addXValue(count + "");

            float yValues = yValue;
            // 位最后一个DataSet添加entry
            lineData.addEntry(new Entry(yValues, count), indexLast);

            chart.notifyDataSetChanged();
            chart.invalidate();
            chart.moveViewTo(yValues, count, YAxis.AxisDependency.LEFT);

            Log.d(TAG, "set.getEntryCount()=" + lastSet.getEntryCount()
                    + " ; indexLastDataSet=" + indexLast);
        }
    }

    private LineDataSet createLineDataSet() {
        LineDataSet dataSet = new LineDataSet(null, "数值");
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setValueTextSize(5f);

        return dataSet;
    }

    /**
     * 获取最后一个LineDataSet的索引
     */
    private int getLastDataSetIndex(LineData lineData) {
        int dataSetCount = lineData.getDataSetCount();
        return dataSetCount > 0 ? (dataSetCount - 1) : 0;
    }

    private void setupTempChart(LineChart chart) {
        chart.setDrawGridBackground(false);
        chart.setDescription("温度变化图表");
        chart.setNoDataTextDescription("暂无数据");
//        chart.setVisibleXRangeMaximum(9);
//        chart.setVisibleYRangeMaximum(150, YAxis.AxisDependency.LEFT);

        // 为chart添加空数据
        chart.setData(new LineData());

        // 设置x轴
        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(4);

        // 设置左侧坐标轴
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);

        // 设置右侧坐标轴
        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(false);
    }

    private void setupHumiChart(LineChart chart) {
        chart.setDrawGridBackground(false);
        chart.setDescription("湿度变化图表");
        chart.setNoDataTextDescription("暂无数据");
//        chart.setVisibleXRangeMaximum(9);
//        chart.setVisibleYRangeMaximum(150, YAxis.AxisDependency.LEFT);

        // 为chart添加空数据
        chart.setData(new LineData());

        // 设置x轴
        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(4);

        // 设置左侧坐标轴
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);

        // 设置右侧坐标轴
        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(false);
    }

    public class LocalReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String response = intent.getStringExtra(AppService.EXTRA_RESPONSE_DATA);
                if (!TextUtils.isEmpty(response)) {
                    // temp=21,humi=56,fire=1
                    for (String s : response.split(",")) {
                        if (s.contains("temp")) {
                            String temp = s.substring(s.indexOf("=") + 1, s.length());
                            tv_temp.setText("当前温度 " + temp + "°C");
                            int i = StringUtil.getNum(temp);
                            addEntry(chartTemp, i);
                        }

                        if (s.contains("humi")) {
                            String hmui = s.substring(s.indexOf("=") + 1, s.length());
                            tv_humi.setText("当前湿度 " + hmui + "°F");
                            int h = StringUtil.getNum(hmui);
                            addEntry(chartHumi, h);
                        }
                    }
                }
                Log.d(TAG, "onReceive: " + response);
            }
            Log.e(TAG, "接收到了本地广播");
        }
    }

}
