package com.meiji.arduinodemo.meiji;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.meiji.arduinodemo.AppService;
import com.meiji.arduinodemo.R;
import com.meiji.arduinodemo.StringUtil;

import static android.content.Context.NOTIFICATION_SERVICE;
import static com.meiji.arduinodemo.meiji.TempFragment.TEMP_IntentFilter;

/**
 * Created by Meiji on 2018/1/6.
 */

public class FireFragment extends Fragment {

    private static final String TAG = "FireFragment";
    static FireFragment INSTANCE;
    TextView tv_fire;
    LineChart chartFire;
    Button bt_openalarm;
    Button bt_closealarm;
    TextView tv_alarm;
    LocalReceiver mLocalReceiver;
    AppService.AppBinder mBinder;
    ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBinder = (AppService.AppBinder) service;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    public static FireFragment newInstance() {
        if (INSTANCE == null) {
            synchronized (FireFragment.class) {
                INSTANCE = new FireFragment();
            }
        }
        return INSTANCE;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fire, container, false);
        initView(view);
        initBC();
        return view;
    }

    void initView(View view) {
        this.tv_fire = view.findViewById(R.id.tv_fire);
        this.chartFire = view.findViewById(R.id.chartFire);
        this.bt_openalarm = view.findViewById(R.id.bt_openalarm);
        this.bt_closealarm = view.findViewById(R.id.bt_closealarm);
        this.tv_alarm = view.findViewById(R.id.tv_alarm);
        setupTempChart(chartFire);
        bt_openalarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinder.sendMessage("1");
                tv_alarm.setText("当前警报状态：开启");
            }
        });

        bt_closealarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinder.sendMessage("0");
                tv_alarm.setText("当前警报状态：关闭");
            }
        });
    }

    void initBC() {

        Intent intent = new Intent(getContext(), AppService.class);
        getContext().bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
        mLocalReceiver = new LocalReceiver();
        LocalBroadcastManager.getInstance(getContext())
                .registerReceiver(mLocalReceiver, new IntentFilter(TEMP_IntentFilter));
    }


    @Override
    public void onDestroy() {
        getContext().unbindService(mServiceConnection);
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mLocalReceiver);
        super.onDestroy();
    }

    public void addEntry(LineChart chart, float yValue) {
        LineData lineData = chart.getData();

        if (lineData != null) {
            int indexLast = getLastDataSetIndex(lineData);
            LineDataSet lastSet = (LineDataSet) lineData.getDataSetByIndex(indexLast);
            // set.addEntry(...); // can be called as well

            if (lastSet == null) {
                lastSet = createLineDataSet();
                lineData.addDataSet(lastSet);
            }
            // 这里要注意，x轴的index是从零开始的
            // 假设index=2，那么getEntryCount()就等于3了
            int count = lastSet.getEntryCount();
            if (count > 20) {
                return;
            }
            // add a new x-value first 这行代码不能少
            lineData.addXValue(count + "");

            float yValues = yValue;
            // 位最后一个DataSet添加entry
            lineData.addEntry(new Entry(yValues, count), indexLast);

            chart.notifyDataSetChanged();
            chart.invalidate();
            chart.moveViewTo(yValues, count, YAxis.AxisDependency.LEFT);

            Log.d(TAG, "set.getEntryCount()=" + lastSet.getEntryCount()
                    + " ; indexLastDataSet=" + indexLast);
        }
    }

    private LineDataSet createLineDataSet() {
        LineDataSet dataSet = new LineDataSet(null, "数值");
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setValueTextSize(5f);
        return dataSet;
    }

    /**
     * 获取最后一个LineDataSet的索引
     */
    private int getLastDataSetIndex(LineData lineData) {
        int dataSetCount = lineData.getDataSetCount();
        return dataSetCount > 0 ? (dataSetCount - 1) : 0;
    }

    private void setupTempChart(LineChart chart) {
        chart.setDrawGridBackground(false);
        chart.setDescription("火焰值变化图表");
        chart.setNoDataTextDescription("暂无数据");
//        chart.setVisibleXRangeMaximum(9);
//        chart.setVisibleYRangeMaximum(150, YAxis.AxisDependency.LEFT);

        // 为chart添加空数据
        chart.setData(new LineData());

        // 设置x轴
        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(4);

        // 设置左侧坐标轴
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);

        // 设置右侧坐标轴
        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(false);
    }

    void showNotification() {

        NotificationManager mNotifyMgr =
                (NotificationManager) getContext().getSystemService(NOTIFICATION_SERVICE);
//        PendingIntent contentIntent = PendingIntent.getActivity(
//                this, 0, new Intent(this, ResultActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getContext())
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentTitle("警报！！！")
                        .setContentText("火焰值过高，已启动警报")
                        .setFullScreenIntent(null, false);

        mNotifyMgr.notify(0, mBuilder.build());

    }

    public class LocalReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String response = intent.getStringExtra(AppService.EXTRA_RESPONSE_DATA);
                if (!TextUtils.isEmpty(response)) {
                    // temp=21,humi=56,fire=1
                    for (String s : response.split(",")) {
                        if (s.contains("fire")) {
                            String temp = s.substring(s.indexOf("=") + 1, s.length());
                            tv_fire.setText("当前火焰值 " + temp);
                            int fire = StringUtil.getNum(temp);
                            addEntry(chartFire, fire);
                            Log.d(TAG, "onReceive: fire = " + fire);

                            if (fire > 50) {
                                showNotification();
                                tv_alarm.setText("当前警报状态：开启");
                            }
                        }
                    }
                }
                Log.d(TAG, "onReceive: " + response);
            }
            Log.e(TAG, "接收到了本地广播");
        }
    }

}
