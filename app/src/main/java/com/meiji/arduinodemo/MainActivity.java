package com.meiji.arduinodemo;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.meiji.arduinodemo.meiji.FireFragment;
import com.meiji.arduinodemo.meiji.TempFragment;

public class MainActivity extends AppCompatActivity {

    final String mServerIP = "192.168.4.1";
    final int mServerPort = 5000;
    AppService.AppBinder mBinder;
    ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBinder = (AppService.AppBinder) service;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };
    private BottomNavigationView nav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initService();

    }

    private void initView() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        nav = findViewById(R.id.nav);
        nav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_temp:
                        changeFragment(TempFragment.newInstance());
                        break;
                    case R.id.action_fire:
                        changeFragment(FireFragment.newInstance());
                }
                return false;
            }
        });
        changeFragment(TempFragment.newInstance());
    }

    void changeFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        for (Fragment f : fragmentManager.getFragments()) {
            transaction.hide(f);
        }
        if (fragmentManager.findFragmentByTag(fragment.getClass().getSimpleName()) == null) {
            transaction.add(R.id.containter, fragment, fragment.getClass().getSimpleName());
        } else {
            transaction.show(fragment);
        }
        transaction.commit();
    }

    void initService() {
        Intent intent = new Intent(this, AppService.class);
        intent.putExtra(AppService.EXTRA_IP, mServerIP);
        intent.putExtra(AppService.EXTRA_PORT, mServerPort);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_connect:
                mBinder.connect();
                break;
            case R.id.action_disconnect:
                mBinder.disConnect();
                break;
            case R.id.action_reconnect:
                mBinder.reConnect();
                break;
            case R.id.action_send:
                showDialog();
                break;
            case R.id.action_receive:
                mBinder.receiverMessage();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        unbindService(mServiceConnection);
        super.onDestroy();
    }

    private void showDialog() {
        final MaterialDialog mDialog = new MaterialDialog.Builder(this)
                .title("发送内容")
                .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS)
                .input("", "", new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {

                    }
                })
                .build();

        // 设置2个按键
        mDialog.setActionButton(DialogAction.NEGATIVE, "取消");
        mDialog.setActionButton(DialogAction.POSITIVE, "发送");

        mDialog.getActionButton(DialogAction.NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.getActionButton(DialogAction.POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String input = mDialog.getInputEditText().getText().toString() + "\n";
                if (!TextUtils.isEmpty(input)) {
                    mBinder.sendMessage(input);
                }
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }
}
