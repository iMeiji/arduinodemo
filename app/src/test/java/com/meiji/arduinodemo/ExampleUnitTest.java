package com.meiji.arduinodemo;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        String s = "149������������������������������������������";
        byte[] bytes = s.getBytes("GBK");
//        String s2 = s.replace("�", "");
        System.out.println(getNum2(s));
        assertEquals(4, 2 + 2);
    }

    int getNum(String str) {
        String str2 = "";
        if (str != null && !"".equals(str)) {
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) >= 48 && str.charAt(i) <= 57) {
                    str2 += str.charAt(i);
                }
            }
        }
        return Integer.parseInt(str2);
    }

    int getNum2(String str) {
        String regex = "[^0-9]";
        Pattern p = Pattern.compile(regex);
        Matcher matcher = p.matcher(str);
        return Integer.parseInt(matcher.replaceAll("").trim());
    }
}